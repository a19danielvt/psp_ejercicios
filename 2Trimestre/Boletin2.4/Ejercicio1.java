import java.util.concurrent.CyclicBarrier;

public class Ejercicio1 {

    final static int N = 4;
    static CyclicBarrier barrier = null;
    
    public static void main(String[] args) {
        barrier = new CyclicBarrier(N);

        for (int i = 0; i < N; ++i) {
            new Thread(new CyclicBarrierWorker()).start();
        }
    }

    static class CyclicBarrierWorker implements Runnable {
        public void run() {
            for(int i = 0; i < 3; ++i) {
                try {
                    long id = Thread.currentThread().getId();
                    System.out.println("Hilo " + id + " haciendo algo");

                    // hace algo
                    Thread.sleep(1000 * (int) (2 * Math.random() * 10));
                    long id1 = Thread.currentThread().getId();
                    System.out.println("Hilo:" + id1 + " ha terminado!!");

                    // esperan hasta que todos los hilos lleguen a este punto
                    barrier.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                System.out.println("Todos han terminado!!");
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
    
        }

    }
}