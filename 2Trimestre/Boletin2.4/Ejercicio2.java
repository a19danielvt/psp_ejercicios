public class Ejercicio2 {
    private static Contenedor contenedor;
    private static Thread productor;
    private static Thread consumidor;
    private static final int CONSUMIDORES = 5;

    public static void main(String[] args) {
        contenedor = new Contenedor();

        productor = new Thread(new Productor(contenedor, 1));
        consumidor = new Thread(new Consumidor(contenedor, 1));
        
        consumidor.start();
        productor.start();
    }
}

class Contenedor {
    private int contenido;
    private boolean contenedorlleno = false;

    public synchronized int get() {
        while (!contenedorlleno) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println("Contenedor: Error en get -> " + e.getMessage());
            }
        }
        contenedorlleno = false;
        notify();
        return contenido;
    }

    public synchronized void put(int valor) {
        while (contenedorlleno) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println("Contenedor: Error en put -> " + e.getMessage());
            }
        }
        contenido = valor;
        contenedorlleno = true;
        notify();
    }
}

class Productor implements Runnable {
    private final Contenedor contenedor;
    private final int idproductor;

    public Productor(Contenedor contenedor, int idproductor) {
        this.contenedor = contenedor;
        this.idproductor = idproductor;
    }

    @Override
    public void run() {
        while (true) {
            int valor = (int)(Math.random() * 300);
            contenedor.put(valor);
            System.out.println("El productor " + idproductor + " pone: " + valor);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.err.println("Productor " + idproductor + ": Error en run -> " + e.getMessage());
            }
        }
    }
}

class Consumidor implements Runnable {
    private final Contenedor contenedor;
    private final int idconsumidor;

    public Consumidor(Contenedor contenedor, int idconsumidor) {
        this.contenedor = contenedor;
        this.idconsumidor = idconsumidor;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println("El consumidor " + idconsumidor + " consume: " + contenedor.get());
        }
    }
}