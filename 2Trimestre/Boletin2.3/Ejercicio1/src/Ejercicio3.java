/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a19danielvt
 */
public class Ejercicio3 {
    private static class MiThread extends Thread{

        static int contador;
        private String cadena;
        
        public MiThread(String cadena){
            contador = 0;
            this.cadena = cadena;
        }
        
        @Override
        public void run() {
            for (int i = 0; i < 10 ; i++){
                System.out.print(++contador + ":" + cadena + " ");
            }
        }
    }
    
    public static void main(String[] args) {
        MiThread hilo1 = new MiThread("SI");
        MiThread hilo2 = new MiThread("NO");
        
        hilo1.start();
        hilo2.start();
    }
}
