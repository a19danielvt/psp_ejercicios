
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a19danielvt
 */
public class Ejercicio4 {
    private static class MiHilo extends Thread{
        String persona;
        static int contador;
        
        public MiHilo(String persona){
            this.persona = persona;
            contador = 0;
        }

        @Override
        public void run() {
            while(true){
                modificarContador();
            }
        }
        
        public synchronized void modificarContador(){
            if(persona.equals("productor")) contador++;
            else{
                if(contador != 0) contador--;
            }
            System.out.println(persona.substring(0, 1) + ": " + contador);
        }
    }
    
    public static void main(String[] args) {
        MiHilo prod = new MiHilo("productor");
        MiHilo cons = new MiHilo("consumidor");
        
        prod.setDaemon(true);
        cons.setDaemon(true);
        
        prod.start();
        cons.start();
        
        try {
            Thread.sleep(25);
        } catch (Exception e) {
        }
    }
}
