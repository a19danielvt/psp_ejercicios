/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a19danielvt
 */
public class Ejercicio1 {
    
    public static void main(String[] args) {
        Runnable runnable = () -> {
            for (int i = 0; i < 30; i++) {
                System.out.print("NO ");
            }
        };
        
        Thread hilo = new Thread(runnable);
        hilo.start();
        
        for (int i = 0; i < 30; i++) {
            System.out.print("YES ");
        }
    }
    
}
