/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a19danielvt
 */
public class Ejercicio2 {
    public static void main(String[] args) {
        Runnable runnable = () -> {
            for (int i = 0; i < 30; i++) {
                System.out.print(Thread.currentThread().getName());
            }
        };

        Thread hilo2 = new Thread(runnable, "NO ");
        Thread hilo1 = new Thread(runnable, "YES ");
        
        System.out.println("Ejecución hilo A");
        hilo1.start();
        System.out.println("Ejecución hilo B");
        hilo2.start();
        
        System.out.println("Ejecución en main");
    }
}
