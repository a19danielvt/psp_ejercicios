import java.io.*;
import java.net.*;

public class Imagen {
    public static void main(String[] args) {
        try {
            URL url = new URL("http://www.lossimpsonsonline.com.ar/la-casa/la-casa.gif");

            URLConnection conn = url.openConnection();

            System.out.println(conn.getContentType());

            InputStream is = conn.getInputStream();
            FileOutputStream fos = new FileOutputStream("foto");

            byte[] array = new byte[1000];
            int leido = is.read(array);
            while(leido > 0){
                fos.write(array, 0, leido);
                leido = is.read(array);
            }

            is.close();
            fos.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
