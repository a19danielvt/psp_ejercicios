import java.net.URL;

public class Ejemplo2 {
    public static void main(String[] args) {
        Ejemplo2 objeto = new Ejemplo2();

        System.out.println("Constructor simple para URL principal");

        try {
            objeto.display(new URL("https://www.youtube.com/"));
            System.out.println("");

            System.out.println("Constructor de cadena para URL + directorio");
            objeto.display(new URL("https://www.youtube.com/watch?v=fpgBs5AFlGg"));
            System.out.println("");

            System.out.println("Constructor con protocolo, host y directorio");
            objeto.display(new URL("http", "www.youtube.com", "/watch?v=fpgBs5AFlGg"));
            System.out.println("");

            System.out.println("Constructor con protocolo, host, puerto y directorio");
            objeto.display(new URL("http", "www.youtube.com", 80, "/watch?v=fpgBs5AFlGg"));
            System.out.println("");
        } catch (Exception e) {
            //TODO: handle exception
        }
    }

    public void display(URL url){
        System.out.println(url.getProtocol());
        System.out.println(url.getHost());
        System.out.println(url.getPort());
        System.out.println(url.getFile());
        System.out.println(url.getRef());
        System.out.println(url.toString());   
    }
}
