import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;

public class Ejemplo3 {
    public static void main(String[] args) {
        String cadena;
        String sFichero = "pagina.html";
        File fichero = new File(sFichero);

        if(fichero.exists()){
            System.out.println("Ya existe");
        }
        else{
            try {
                URL url = new URL("https://es.wikipedia.org/wiki/Copa_del_Rey_de_f%C3%BAtbol_2020-21");
                BufferedReader pagina = new BufferedReader(new InputStreamReader(url.openStream()));
                BufferedWriter bw = new BufferedWriter(new FileWriter(sFichero));

                while((cadena = pagina.readLine()) != null){
                    bw.write(cadena + "\r\n");
                }

                System.out.println("Fichero creado correctamente");
                pagina.close();
                bw.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }            
        }
    }
}
