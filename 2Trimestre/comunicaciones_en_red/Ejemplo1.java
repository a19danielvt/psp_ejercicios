import java.net.InetAddress;
import java.net.UnknownHostException;

public class Ejemplo1{

    public static void main(String[] args) {
        try {
            System.out.println("-> Dirección IP de una URL, por nombre:");
            InetAddress address = InetAddress.getByName("www.iessanclemente.net");
            System.out.println(address);
            System.out.println("");

            System.out.println("-> Nombre a partir de la dirección: ");
            int temp = address.toString().indexOf('/');
            address = InetAddress.getByName(address.toString().substring(temp + 1));
            System.out.println(address);
            System.out.println("");

            System.out.println("-> Nombre del localhost a partir de la dirección: ");
            address = InetAddress.getLocalHost();
            System.out.println(address);
            System.out.println("");

            System.out.println("-> Nombre actual de localhost: ");
            byte[] bytes = address.getAddress();
            for (int i = 0; i < bytes.length; i++) {
                int uByte = bytes[i] < 0 ? bytes[i] + 256 : bytes[i];
                System.out.print(uByte + " ");
            }
        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());
        }
    }
}