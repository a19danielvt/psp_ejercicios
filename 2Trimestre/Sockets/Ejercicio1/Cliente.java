import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Cliente {
    public static void main(String[] args) {
        String host = "localhost";
        int puerto = 6000;
        Socket cliente;
        DataOutputStream flujoSalida;
        DataInputStream flujoEntrada;

        try {
            System.out.println("PROGRAMA CLIENTE INICIADO .....");
            cliente = new Socket(host, puerto);

            while(true){
                // FLUJO DE SALIDA AL SERVIDOR
                flujoSalida = new DataOutputStream(cliente.getOutputStream());

                // ENVIO UN SALUDO AL SERVIDOR
                Scanner sc = new Scanner(System.in);
                System.out.print("Introduce un número: ");
                flujoSalida.writeUTF(sc.next());

                // FLUJO DE ENTRADA AL SERVIDOR
                flujoEntrada = new DataInputStream(cliente.getInputStream());

                // EL SERVIDOR ME ENVIO UN MENSAJE
                boolean correcto = false;
                String mensajeServidor = flujoEntrada.readUTF();

                if(mensajeServidor.equals("ACERTASTE!")) correcto = true;

                System.out.println(mensajeServidor + "\n");
                if(correcto) break;
            }

            // CERRAR STREAMS Y SOCKETS
            flujoEntrada.close();
            flujoSalida.close();
            cliente.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
