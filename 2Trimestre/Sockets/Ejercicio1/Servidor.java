import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
    public static void main(String[] args) {
        ServerSocket servidor;
        Socket clienteConectado;
        DataInputStream flujoEntrada;
        DataOutputStream flujoSalida;

        int numeroPuerto = 6000;
        try {
            int numero = (int)(Math.random() * 101);
            servidor = new ServerSocket(numeroPuerto);
            System.out.println("Esperando al cliente .....");
            clienteConectado = servidor.accept();

            while(true){
                
                // FLUJO DE ENTRADA DEL CLIENTE
                flujoEntrada = new DataInputStream(clienteConectado.getInputStream());
    
                // EL CLIENTE ME ENVÍA UN MENSAJE
                String mensaje = "";
                boolean correcto = false;
                
                int nCliente = Integer.parseInt(flujoEntrada.readUTF());

                if(nCliente < numero){
                    mensaje = "El número es mayor";
                }else if(nCliente > numero){
                    mensaje = "El número es menor";
                }else{
                    mensaje = "ACERTASTE!";
                    correcto = true;
                }
    
                // FLUJO DE SALIDA AL CLIENTE
                flujoSalida = new DataOutputStream(clienteConectado.getOutputStream());
    
                // ENVIO UN SALUDO AL CLIENTE
                flujoSalida.writeUTF(mensaje);
                if(correcto) break;
            }

            // CERRAR STREAMS Y SOCKETS
            flujoEntrada.close();
            flujoSalida.close();
            clienteConectado.close();
            servidor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
