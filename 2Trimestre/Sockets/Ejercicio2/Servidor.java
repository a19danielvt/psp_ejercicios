import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
    public static void main(String[] args) {
        ServerSocket servidor;
        Socket clienteConectado;
        DataInputStream flujoEntrada;
        DataOutputStream flujoSalida;

        int numeroPuerto = 1500;
        try {
            servidor = new ServerSocket(numeroPuerto);
            System.out.println("Esperando al cliente .....");
            clienteConectado = servidor.accept();

            while(true){
                
                // FLUJO DE ENTRADA DEL CLIENTE
                flujoEntrada = new DataInputStream(clienteConectado.getInputStream());
    
                // EL CLIENTE ME ENVÍA UN MENSAJE
                String mensaje = "NO ES UN ARCHIVO";
                boolean correcto = false;
                
                File file = new File(flujoEntrada.readUTF());

                if(file.exists() && file.isFile()){
                    BufferedReader bf = new BufferedReader(new FileReader(file));

                    String linea;

                    while((linea = bf.readLine()) != null){
                        mensaje += linea + "\n";
                    }

                    bf.close();
                    correcto = true;
                }
    
                // FLUJO DE SALIDA AL CLIENTE
    
                flujoSalida = new DataOutputStream(clienteConectado.getOutputStream());
    
                // ENVIO UN SALUDO AL CLIENTE
                flujoSalida.writeUTF(mensaje);
                if(correcto) break;
            }

            // CERRAR STREAMS Y SOCKETS
            flujoEntrada.close();
            flujoSalida.close();
            clienteConectado.close();
            servidor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
