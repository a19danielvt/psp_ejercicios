import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Cliente {
    public static void main(String[] args) {
        String host = "localhost";
        int puerto = 1500;
        Socket cliente;
        DataOutputStream flujoSalida;
        DataInputStream flujoEntrada;

        try {
            System.out.println("PROGRAMA CLIENTE INICIADO .....");
            cliente = new Socket(host, puerto);

            while(true){
                // FLUJO DE SALIDA AL SERVIDOR
                flujoSalida = new DataOutputStream(cliente.getOutputStream());

                // ENVIO UN SALUDO AL SERVIDOR
                Scanner sc = new Scanner(System.in);
                System.out.print("Introduce una ruta: ");
                flujoSalida.writeUTF(sc.nextLine());

                // FLUJO DE ENTRADA AL SERVIDOR
                flujoEntrada = new DataInputStream(cliente.getInputStream());

                // EL SERVIDOR ME ENVIO UN MENSAJE
                boolean correcto = true;
                String mensajeServidor = flujoEntrada.readUTF();

                if(mensajeServidor.equals("NO ES UN ARCHIVO")) correcto = false;

                System.out.println(mensajeServidor + "\n");
                if(correcto) break;
            }

            // CERRAR STREAMS Y SOCKETS
            flujoEntrada.close();
            flujoSalida.close();
            cliente.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
