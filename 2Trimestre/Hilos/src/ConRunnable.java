/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipman
 */
public class ConRunnable {
    
    public static class MyRunnable implements Runnable{
        @Override
        public void run() {
            System.out.println("Hola con runnable");
        }
    }
    
    public static void main(String[] args) {
        Thread hilo = new Thread(new MyRunnable());
        hilo.start();
    }
}
