/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipman
 */
public class DetenerHilos {
    public static class StoppableRunnable implements Runnable{
        
        private boolean stopRequested = false;

        public synchronized void requestStop(){
            this.stopRequested = true;
        }
        
        public synchronized boolean isStopRequested(){
            return this.stopRequested;
        }
        
        private void sleep(long millis){
            try {
                Thread.sleep(millis);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        
        @Override
        public void run() {
            while(!isStopRequested()){
                sleep(1000);
                System.out.println("...");
            }
            System.out.println("FIN");
        }
    }
    
    public static void main(String[] args) {
        StoppableRunnable runnable = new StoppableRunnable();
        Thread hilo = new Thread(runnable);
        hilo.start();
        
        runnable.sleep(5000);
        
        runnable.requestStop();
    }
}
