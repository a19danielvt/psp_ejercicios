/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipman
 */
public class HiloEsperaHilo {
    public static void main(String[] args) {
        Runnable runnable = () -> {
            for (int i = 0; i < 5; i++) {
                sleep(1000);
                System.out.println("Funcionando");
            }
        };
        
        Thread hilo = new Thread(runnable);
        hilo.setDaemon(true);
        hilo.start();
        
        try {
            hilo.join();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void sleep(int s){
        try {
            Thread.sleep(s);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
