/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipman
 */
public class Demonios {
    public static void main(String[] args) {
        Runnable runnable = () -> {
            while(true){
                sleep(1000);
                System.out.println("Hola");
            }
        };

        Thread hilo = new Thread(runnable);
        hilo.setDaemon(true);
        hilo.start();
        sleep(3100);
    }
    
    private static void sleep(int i){
        try {
            Thread.sleep(i);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
