import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import java.awt.event.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipman
 */
public class UsoHilos {
    public static void main(String[] args) {
        JFrame frame = new MarcoRebote();
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

class PelotaHilos implements Runnable{
    
    private Pelota pelota;
    private Component c;
    
    public PelotaHilos(Pelota pelota, Component c){
        this.pelota = pelota;
        this.c = c;
    }

    @Override
    public void run() {
        for (int i = 0; i < 3000; i++) {
            pelota.muevePelota(c.getBounds());
            c.paint(c.getGraphics());
            
            try {
                Thread.sleep(10);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
    
}

class Pelota {
    public void muevePelota(Rectangle2D limites){
        x += dx;
        y += dy;

        if(x < limites.getMinX()){
            x = limites.getMinX();
            dx *= -1;
        }

        if(x + TAMX>=limites.getMaxX()){
            x = limites.getMaxX() - TAMX;
            dx *= -1;
        }

        if(y < limites.getMinY()){
            y = limites.getMinY() ;
            dy *= -1;
        }

        if(y + TAMY >= limites.getMaxY()) {
            y = limites.getMaxY() - TAMY;
            dy *= -1;
        }
    }

    public Ellipse2D getShape() {
        return new Ellipse2D.Double(x, y, TAMX, TAMY);
    }

    private static final int TAMX = 15;
    private static final int TAMY = 15;
    private double x = 0;
    private double y = 0;
    private double dx, dy = 1;  
}

class LaminaPelota extends JPanel{
    
    private ArrayList<Pelota> pelotas = new ArrayList<>();
    
    public synchronized void add(Pelota b){
        pelotas.add(b);
    }
    
    @Override
    public synchronized void paintComponent(Graphics g){
        super.paintComponent(g);
        
        Graphics2D g2 = (Graphics2D) g;
        
        for (Pelota b : pelotas) {
            g2.fill(b.getShape());
        }
    }
    
    public ArrayList<Pelota> getPelotas(){
        return pelotas;
    }
}

class MarcoRebote extends JFrame {
    
    private LaminaPelota lamina;
    
    public MarcoRebote() {
        setBounds(600 ,300, 400, 350);
        setTitle("Rebotes");
        
        lamina = new LaminaPelota();
        add(lamina, BorderLayout.CENTER);
        
        JPanel laminaBotones = new JPanel();
        ponerBoton(laminaBotones, "Dale!", new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ev){
                comienzaElJuego();
            }
        });
        ponerBoton(laminaBotones, "Salir", new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ev){
                System.exit(0);
            }
        });
        add(laminaBotones, BorderLayout.SOUTH);
    }
    
    public void ponerBoton(Container c, String titulo, ActionListener oyente){
        JButton boton = new JButton(titulo);
        c.add(boton);
        boton.addActionListener(oyente);
    }
    
    public void comienzaElJuego(){
        Pelota pelota = new Pelota();
        lamina.add(pelota);
        
        Runnable runnable = new PelotaHilos(pelota, lamina);
        Thread hilo = new Thread(runnable);
        hilo.start();
    }    
}