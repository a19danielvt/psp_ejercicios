import java.net.*;

/**
 * Ejercicio8
 */
public class Ejercicio8 {

    public static void main(String[] args) {
        if(args.length == 0){
            System.out.println("Introduce una URL");
            System.exit(0);
        }
        
        try {
            InetAddress inet = InetAddress.getByName(args[0]);
            System.out.println("Name: " + args[0]);
            System.out.println("Address: " + inet.getHostAddress());
        } catch (Exception e) {
            System.out.println("No existe la dirección web");
        }
    }
}