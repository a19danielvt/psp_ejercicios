import java.net.*;

/**
 * Ejercicio7
 */
public class Ejercicio7 {

    public static void main(String[] args) {
        try {
            String host = "www.wikipedia.org";
            InetAddress inet = InetAddress.getByName(host);
            System.out.println(inet.getHostAddress());
            System.out.println(inet.getHostName());

            System.out.println(InetAddress.getLocalHost());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}