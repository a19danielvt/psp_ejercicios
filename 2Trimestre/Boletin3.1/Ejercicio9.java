import java.net.*;

/**
 * Ejercicio9
 */
public class Ejercicio9 {

    public static void main(String[] args) {
        if(args.length == 0){
            System.out.println("Introduce una IP");
            System.exit(0);
        }
        
        try {
            InetAddress inet = InetAddress.getByName(args[0]);
            System.out.println("Name: " + inet.getHostName());
            System.out.println("Address: " + args[0]);
        } catch (Exception e) {
            System.out.println("No existe la dirección web");
        }
    }
}