import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class Monitores {
    public static void main(String[] args) {
        //premios
        CajaPremios caja = new CajaPremios();
        String[] arrayPremios = {"premio1", "premio2", "premio3", "premio4", "premio5"};
        int[] premiados = {2, 7, 4, 9, 12};
        
        for (int i = 0; i < 5; i++) {
            caja.addPremio(new Premio(premiados[i], arrayPremios[i]));
        }

        Sorteo sorteo = new Sorteo(caja);

        //socios
        for (int i = 0; i < 15; i++) {
            int random = 1 + (int)(Math.random() * 15);
            System.out.println(random);
            Socio socio = new Socio(i + 1, random);
            
            Thread hilo = new Thread(){
                public void run(){
                    sorteo.comprobar(socio);
                }
            };
            hilo.start();
        }
    }
}


class Sorteo{
    CajaPremios caja;
    Semaphore comprobar = new Semaphore(1);

    public Sorteo(CajaPremios caja){
        this.caja = caja;
    }

    public void comprobar(Socio socio){

        try {
            comprobar.acquire();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } 

        for (Premio premio : caja.getPremios()) {
            if(premio.getNumber() == socio.getRifa()){
                recogerPremio(socio, premio);
                comprobar.release();
                return;
            }
        }

        comprobar.release();
    }

    public void recogerPremio(Socio socio, Premio premio){

        if(caja.sacarPremio(premio))
            System.out.println(socio.getId() + " ha ganado el " + premio.getPremio());

    }
}


class Socio{
    private int id;
    private int rifa;

    public Socio(int id, int rifa){
        this.id = id;
        this.rifa = rifa;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRifa() {
        return rifa;
    }

    public void setRifa(int rifa) {
        this.rifa = rifa;
    }
}


class Premio {
    private int number;
    private String premio;

    public Premio(int number, String premio){
        this.number = number;
        this.premio = premio;
    }

    public int getNumber() {
        return number;
    }

    public String getPremio() {
        return premio;
    }
}


class CajaPremios{
    private ArrayList<Premio> premios = new ArrayList<>();

    public void addPremio(Premio premio){
        premios.add(premio);
    }

    public boolean sacarPremio(Premio premio){
        return premios.remove(premio);
    }

    public ArrayList<Premio> getPremios() {
        return premios;
    }
}