import java.util.concurrent.Semaphore;

public class Semaforos2 {
    public static void main(String[] args) {
        Ejemplo ejemplo = new Ejemplo();

        Thread hilo1 = new Thread(ejemplo, "hilo1");
        Thread hilo2 = new Thread(ejemplo, "hilo2");
        Thread hilo3 = new Thread(ejemplo, "hilo3");
        Thread hilo4 = new Thread(ejemplo, "hilo4");

        hilo1.start();
        hilo2.start();
        hilo3.start();
        hilo4.start();
    }
}


class Ejemplo implements Runnable{

    Semaphore semaforo = new Semaphore(1);

    @Override
    public void run() {
        try {
            semaforo.acquire();
            System.out.println("Ejecutado por: " + Thread.currentThread().getName());
            for (int i = 0; i < 5; i++) {
                System.out.println("El bucle lo hace: " + Thread.currentThread().getName());
            }
            semaforo.release();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}