import java.util.concurrent.Semaphore;

/**
 * Semaforos1
 */
public class Semaforos1 {

    public static void main(String[] args) {
        Semaphore semaforo = new Semaphore(1);

        try {
            semaforo.acquire(); //resta
            semaforo.release(); //suma
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println("Permisos disponibles: " + semaforo.availablePermits());
    }
}
