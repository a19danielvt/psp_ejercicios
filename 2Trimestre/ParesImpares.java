import java.util.concurrent.Semaphore;

public class ParesImpares {
    public static void main(String[] args) {
        GeneradorParImpar parImpar = new GeneradorParImpar();

        GeneradorImpares impares = new GeneradorImpares(parImpar, 10);
        GeneradorPares pares = new GeneradorPares(parImpar, 10);

        Thread hilo1 = new Thread(impares);
        Thread hilo2 = new Thread(pares);

        hilo1.start();
        hilo2.start();
    }
}

class GeneradorParImpar{
    Semaphore impar = new Semaphore(1);
    Semaphore par =  new Semaphore(0);

    public void imprimeImpar(int num){
        try {
            impar.acquire();
            System.out.println(num);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } 
        par.release();
    }

    public void imprimePar(int num){
        try {
            par.acquire();
            System.out.println(num);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } 
        impar.release();
    }
}

class GeneradorImpares implements Runnable{
    private GeneradorParImpar generadorParImpar;
    private int total;

    public GeneradorImpares(GeneradorParImpar generadorParImpar, int total){
        this.generadorParImpar = generadorParImpar;
        this.total = total;
    }

    @Override
    public void run() {
        for (int i = 1; i <= total; i += 2) {
            generadorParImpar.imprimeImpar(i);
        }
    }
}

class GeneradorPares implements Runnable{
    private GeneradorParImpar generadorParImpar;
    private int total;

    public GeneradorPares(GeneradorParImpar generadorParImpar, int total){
        this.generadorParImpar = generadorParImpar;
        this.total = total;
    }

    @Override
    public void run() {
        for (int i = 2; i <= total; i += 2) {
            generadorParImpar.imprimePar(i);
        }
    }
}