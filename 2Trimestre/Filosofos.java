public class Filosofos {
    public static void main(String args[]) {
        System.out.println("Cena de los Filosofos");
        Cena cena = new Cena();
        for (int i = 0; i < 5; i++) {
            Thread hilo = new Thread(new Filosofo(i, cena));
            hilo.start();
        }
    }
}

class Cena {
    Tenedor tenedores[];
    int parametro = 5;

    public Cena() {
        tenedores = new Tenedor[parametro];
        for (int i = 0; i < 5; i++) {
            tenedores[i] = new Tenedor(i);
        }
    }

    public Tenedor getTenedor(int x) {
        return tenedores[x];
    }

    public int getTenedorDer(int x) {
        return (x + 1) % parametro;
    }

    public int getTenedorIzq(int x) {
        return x;
    }

}


class Tenedor {

    int numero;
    boolean enUso;

    public Tenedor(int x) {
        numero = x;
    }

    synchronized public boolean usar() {
        if (enUso) {
            System.out.println("Tenedor " + numero + " esta en uso, espera");
            return false;
        } else {
            enUso = true;
            System.out.println("Se esta usando el tenedor " + numero);
            return true;
        }
    }

    synchronized public void dejar() {
        enUso = false;
        System.out.println("Tenedor " + numero + " esta ahora disponible");
    }

}


class Filosofo implements Runnable {

    protected Cena cena;
    protected int tizq, tder;
    protected int numero;

    public Filosofo(int x, Cena cena) {
        this.numero = x;
        this.cena = cena;
        tizq = cena.getTenedorIzq(numero);
        tder = cena.getTenedorDer(numero);
    }

    public void run() {
        while (true) {
            pensar();
            if(!tomarTenedores()) continue;
            comer();
            dejarTenedores();
        }
    }

    public void pensar() {
        try {
            System.out.println("Filosofo " + numero + " pensando");
            int espera = (int) (Math.random() * 50);
            Thread.sleep(espera);
            System.out.println("Filosofo " + numero + " tiene hambre");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean tomarTenedores() {
        System.out.println("Filósofo " + numero + " tomando tenedores");
        Tenedor t1 = cena.getTenedor(tizq);
        Tenedor t2 = cena.getTenedor(tder);
        if(t1.usar() && t2.usar()){
            return true;
        }
        return false;
    }

    public void comer() {
        try {
            System.out.println("Filosofo " + numero + " esta comiendo");
            int espera = (int) (Math.random() * 100);
            Thread.sleep(espera);
            System.out.println("Filosofo " + numero + " esta satisfecho");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void dejarTenedores() {
        System.out.println("Soltando los tenedores.");
        Tenedor t1 = cena.getTenedor(tizq);
        Tenedor t2 = cena.getTenedor(tder);
        t1.dejar();
        t2.dejar();
    }
}