/**
 * Clase donde reside el método main() que inicia la aplicación
 */
public class ProgramaFilosofos implements IProgramaFilosofos{

    //////////////////////////// ATRIBUTOS /////////////////////////////////////
    


    //////////////////////////// SETTERS Y GETTERS /////////////////////////////
    @Override
    public IFilosofo[] getFilosofos() {
        //TODO:
        return null;
    }

    @Override
    public int getNumeroFilosofos() {
        //TODO:
        return 0;
    }

    @Override
    public void setNumeroFilosofos(int numeroFilosofos) {
        //TODO:
    }

    //////////////////////////// CONSTRUCCIÓN //////////////////////////////////
    
    ProgramaFilosofos(){
        //Valores default
        this.setNumeroFilosofos(5);
    }

    /**
     * Define los parámetros necesarios para construir hilos filósofos (ver el
     * constructor de la clase Filosofo). Se crean los 5 filósofos, y los inicia.
     *
     * @param args [0] El número de filósofos que participan
     */
    ProgramaFilosofos(String[] args){
        this.procesaParametros(args);
        this.construyeEscenario();
    }

    //////////////////////////// COMPORTAMIENTO ////////////////////////////////
    private void procesaParametros(String[] args){
        //TODO:
    }

    private void construyeEscenario() {
        //TODO:
    }

    @Override
    public void comenzar(){
        //TODO:
    }

    //////////////////////////// ARRANQUE //////////////////////////////////////
    public static void main(String[] args) {

        if(args.length == 0){
            // Valores por defecto
            args = new String[1];
            args[0] = String.valueOf(5); // Número de filósofos
        }

        new ProgramaFilosofos(args).comenzar();;
    }
}