/**
 * Hilo filósofo. Su método run() realiza un bucle infinito que consite en
 * invocar a los métodos pensar y comer
 * ... -> pensar -> comer...
 *
 * @author IMCG
 */
public class Filosofo implements IFilosofo{

    //////////////////////////// ATRIBUTOS /////////////////////////////////////

    private Long numeroBocados;
    private String nombre;
    private IFilosofo.Estado estado;
    private boolean hablador;
    private Long millisMaxPensando;
    private Long millisMaxEntreBocadoAndBocado;


    //////////////////////////// SETTERS Y GETTERS /////////////////////////////
    public Long getNumeroBocados() {
        return numeroBocados;
    }

    public String getNombre() {
        return nombre;
    }

    public IFilosofo.Estado getEstado() {
        return estado;
    }

    public boolean isHablador() {
        return hablador;
    }

    public void setHablador(boolean hablador) {
        this.hablador = hablador;
    }

    public Long getMillisMaxPensando() {
        return millisMaxPensando;
    }

    public void setMillisMaxPensando(long millisMaxPensando) {
        this.millisMaxPensando = millisMaxPensando;
    }

    public Long getMillisMaxEntreBocadoAndBocado() {
        return millisMaxEntreBocadoAndBocado;
    }

    public void setMillisMaxEntreBocadoAndBocado(long millisMaxEntreBocadoAndBocado) {
        this.millisMaxEntreBocadoAndBocado = millisMaxEntreBocadoAndBocado;
    }

    //////////////////////////// CONSTRUCCIÓN //////////////////////////////////

    /**
     * constructor de tres parámetros, cada uno de los cuales se guardará en una
     * variable local para usarla cuando sea neceario
     * @param pNombreFilosofo el nombre que le queremos dar al filósofo (puede ser cualquiera)
     * @param params [0] miIndice: índice que identifica al filósofo (un entero del 0 al 4)
     * @param params [1] semaforoPalillo: vector de semáforos (uno para cada palillo).
     * @param params [2] palillosFilosofo: matriz que para cada valor de su primer
     * índice, la fila, almacena los palillos que necesita el filósofo de ese
     * índice para comer. Por ejemplo: el filósofo de índice 0 necesita los
     * palillos de índices {0,4}, el de índice 1 los de índices {1,0}, etc...
     * Puedes prescindir de este vector si se te ocurre como calcular en tiempo
     * real los índices de los palillos que necesita cada filósofo para comer
     */
    public Filosofo(String pNombreFilosofo, Object... params){
        //TODO:
    }


    //////////////////////////// COMPORTAMIENTO ////////////////////////////////
    /**
     * bucle infinito: llamada al método pensar(), llamada al método comer()
     */
    @Override
    public void run() {

        while (true) {
            pensar();
            comer();
        }
    }

    /**
     * método pensar(): mostrará un mensaje en la Salida de que el
     * 'Filósofo ' N ' está pensado'.
     * Para simular esta actividad, dormirá el hilo un tiempo aleatorio
     * 
     */
    public void pensar() {
        //TODO:
    }

    /**
     * método comer(): mostrará un mensaje en la Salida de que el
     * 'Filósofo ' N ' está hambriento', mientras trata de conseguir los
     * dos palillos que necesita para comer. Una vez conseguidos,
     * mostrará un mensaje de que el 'Filósofo ' N ' está comiendo'.
     * Para simular esta actividad, dormirá el hilo un tiempo aleatorio. Cuando
     * termine, mostrará un mensaje de que el 'Filósofo ' N ' ha
     * terminado de comer', indicando los palillos que se quedan libres.
     */
    public void comer() {
        
    }

    public void hablar(String pMensaje){
        //TODO:
    }

    @Override
    public void comienza() {
        // TODO Auto-generated method stub

    }

    @Override
    public void retirate() {
        // TODO Auto-generated method stub

    }

    @Override
    public void interrumpete() {
        // TODO Auto-generated method stub

    }
}