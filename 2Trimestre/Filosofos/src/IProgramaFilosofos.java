public interface IProgramaFilosofos {

    /** Comienzan los filósofos a actual*/
    public void comenzar();

    /** Retorna un array de filósofos
     * @return  */
    IFilosofo[] getFilosofos();

    /** Retorna el número de filósofos creados por el programa
     * @return  */
    public int getNumeroFilosofos();

    /** Establece antes de comentar la cena de los filósofos el número que participarán
     * @param numeroFilosofos */
    public void setNumeroFilosofos(int numeroFilosofos);
}