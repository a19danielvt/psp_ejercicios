import java.io.*;

public class AbrirDir {
    public static void main(String[] args) {
        Runtime r = Runtime.getRuntime();
        String[] comando = {"ping", "www.google.es"};
        Process p = null;
        try{
            p = r.exec(comando);
            InputStream is = p.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String linea;
            
            while((linea = br.readLine()) != null){
                System.out.println(linea);
            }
            
            br.close(); is.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
        int exitVal;
        try{
            exitVal = p.waitFor();
            System.out.println("Valor salida: " + exitVal);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
