import java.io.InputStream;

public class Ejercicio3 {
    public static void main(String[] args) {
        try {
            ProcessBuilder pb = new ProcessBuilder("ping", args[0]);
            Process p = pb.start();

            InputStream is = p.getInputStream();
            
            int i;
            while((i = is.read()) != -1){
                System.out.print((char) i);
            }
        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}
