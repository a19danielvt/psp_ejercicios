import java.util.*;
import java.util.concurrent.TimeUnit;
import java.io.*;

public class Ejercicio5 {
    public static void main(String[] args) {
        try {
            ProcessBuilder pb = new ProcessBuilder("java", "Mayusculas");
            Process p = pb.start();

            InputStream is = System.in;
            OutputStream os = p.getOutputStream();

            int i;
            while((i = is.read()) != -1){
                os.write(i);
            }

            is = p.getInputStream();

            while((i = is.read()) != -1){
                System.out.print((char) i);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
