import java.io.*;
import java.util.Scanner;

public class Ejercicio4 {
    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            ProcessBuilder pb = new ProcessBuilder("java", "Aleatorios");
            while(true) {
                System.out.print("String: ");

                String text = sc.next();

                if(text.equals("fin"))
                    break;

                Process p = pb.start();
                p.waitFor();

                InputStream is = p.getInputStream();
                int i;
                while((i = is.read()) != -1){
                    System.out.print((char) i);
                }

                is.close();
            }
            sc.close(); 
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }    
}

