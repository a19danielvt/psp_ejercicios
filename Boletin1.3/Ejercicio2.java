import java.io.InputStream;

public class Ejercicio2 {
    public static void main(String[] args) {
        try {
            String[] comandos = args;
            ProcessBuilder pb = new ProcessBuilder(comandos);
            Process p = pb.start();
    
            InputStream errores = p.getErrorStream();    

            int i;

            while((i = errores.read()) != -1){
                System.out.print((char) i);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
