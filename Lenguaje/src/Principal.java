import java.io.*;

public class Principal {
    public static void main(String[] args) {
        try{
            FileWriter fo = new FileWriter(new File(args[1]));
            
            int palabras = Integer.parseInt(args[0]);
            
            int letras = (args.length == 2)? (palabras * 3) : Integer.parseInt(args[2]);
                
                
            for(int i = 0; i < palabras; i++){
                for(int j = 0; j < letras ;j++){
                    int grupo = (int)(Math.random() * 2);
                    char letra = ' ';
                    if(grupo == 0)
                        letra = (char)((int)(65 + Math.random() * 25));
                    else 
                        letra = (char)((int)(97 + Math.random() * 25));
                    
                    fo.write(letra);
                }
                fo.write("\n");
            }
            
            fo.close();
        }catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }
}
