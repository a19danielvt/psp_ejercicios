
import java.io.*;

public class EjecutarJava {
    public static void main(String[] args) {
        try {
            //File ruta = new File("/home/jefe/2DAM/psp_ejercicios/ClaseViernes/src/Suma.java");

            ProcessBuilder pb = new ProcessBuilder("java", "Suma.java");
            Process p = pb.start();

            InputStream is = p.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String linea;
            while((linea = br.readLine()) != null){
                System.out.print(linea);
            }
        } catch (Exception e) {
        }
    }
}
