
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AbrirProcesos {
    public static void main(String[] args) {
        String [] comandos = {"gnome-terminal"};
        ProcessBuilder pb = new ProcessBuilder(comandos);
        try {
            Process p = pb.start();
        } catch (IOException ex) {
            Logger.getLogger(AbrirProcesos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
