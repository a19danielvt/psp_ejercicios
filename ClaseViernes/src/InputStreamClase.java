import java.io.*;

public class InputStreamClase {
    public static void main(String[] args) {
        try {
            ProcessBuilder pb = new ProcessBuilder("cal", "-d", "2001-2");
            Process p = pb.start();
            
            InputStream is = p.getInputStream();
      	    BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String linea;
            
            while((linea = br.readLine()) != null){
                System.out.println(linea);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
