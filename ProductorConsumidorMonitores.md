#Problema del Productor-Consumidor con monitores

~~~
Buffer buffer = new Buffer();

Productor(){
	char x;
	while(true){
		producir(x);
		buffer.añadir(x);
	}
}

Consumidor(){
	char x;
	while(true){
		buffer.tomar(x);
		consumir(x);
	}
}
~~~

~~~
Buffer monitor = new Buffer();

char buffer[TAM_BUFFER] -> espacio para N elementos
int entra, sale; -> apuntados al buffer
int contador -> n elementos que hay en el buffer
condition no_lleno, no_vacio; -> variables de condición

añadir(char x){
	if(contador == TAM_BUFFER){
		waitC(no_lleno(
	}
	else{
		buffer[entra] = x;
		entra++;
		contador++;
		signalC(no_vacio);
	}
}

tomar(char x){
	if(contador == 0){
		waitC(no_vacío);
	}
	else{
		x = buffer[sale];
		sale++;
		contador--;
		signal(no_lleno);
	}
}
~~~
