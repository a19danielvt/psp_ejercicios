public class DestroyDemo {
    public static void main(String[] args) {
        try{
            System.out.println("Creando proceso...");
            
            ProcessBuilder pb = new ProcessBuilder("gnome-terminal");
            Process p = pb.start(); // inicia el proceso
            
            System.out.println("Esperamos...");
            
            Thread.sleep(3000); // espera 3 seg
            
            p.destroy(); // destruye el proceso
            System.out.println("Proceso ha muerto...");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
