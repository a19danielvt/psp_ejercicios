Tenemos procesos que producen información y la llevan a memoria, y otros que cogen la información de la memoria.

>Es un paradigma de procesos cooperantes en el que el productor produce una información que es consumida por un proceso consumidor.<br>
>Suponemos que uno o más productores generan cierto tipo de datos y los van situando en un buffer. El consumidor saca elementos del buffer de uno en uno.<br>
>El sistema está obligado a impedir la superposición de operaciones sobre el buffer, por lo tanto, el buffer  va a ser sección crítica.<br>
>Tenemos que asegurarnos de que si el productor está escribiendo un elemento en el buffer, el consumidor no debe tener acceso a ese buffer y viceversa.<br>
>El productor puede generar elementos y almacenarlos en el buffer a su propio ritmo, es decir, puede ser más o menos rápido que el consumidor.<br>
>El consumidor procede de manera similar pero debe estar seguro de que no intenta leer un buffer vacío, por lo tanto, el consumidor debe asegurarse de que el productor ha progresado por delante de él, es decir, que las entradas sean mayores que las salidas antes de continuar.

#### Propuesta de solución

~~~java
//variables

int n;		//número de elementos que hay en el buffer, producidos por el productor
sem s;		//cuida la sección crítica
sem delay;	//cuando el consumidor tome todos los elementos del buffer y no haya más que tomar (n == 0), se bloquea el consumidor hasta que el productor añada
~~~

~~~java
//proceso productor

productor(){
    while(true){
        produzca;
	down(s);
	añadir;
	n++;

	if(n == 1){
	    up(delay); //hay un elemento disponible dentro del buffer;
	    up(s);
	}
    }
}
~~~

~~~java
//proceso consumidor

consumidor(){
    down(delay);
    while(true){
	down(s);
	tomar;
	n--;
	up(s);
	consumir;

	if(n == 0){
	    down(delay);
	}
    }
}
~~~

~~~java
//main

n = 0;
s = 1;
delay = 0;

begin
    productor();
    consumidor();
end
~~~

Esta solución no funionaría ya que nos da un problema de exclusión mutua. El fallo está en el consumidor.

Decrece la variable n, y antes de comprobarla, hacemos un up al semáforo s. Así, permito que el productor "añada", incrementando la variable n. Es decir, decremento la variable pero ante de compararla permito que el otro proceso la modifique.

Modificamos el consumidor:

~~~java
consumidor(){
    int temp;
    down(delay);
    while(true){
	down(s);
	tomar;
	n--;
	m = n;
	up(s);
	consumir;
	if(m == 0){
	    down(delay);
	}
    }
}
~~~

Esta es la solución correcta.
