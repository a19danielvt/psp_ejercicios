
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExitValueDemo {
    public static void main(String[] args) throws Exception{
        System.out.println("Creando proceso...");

        ProcessBuilder pb = new ProcessBuilder("gedit");
        Process p = pb.start();

        Thread.sleep(2000);

        int valor = p.exitValue();

        if(valor != 0){
            throw new Exception("Valor: " + valor);
        }
        p.destroy();
    }
}
