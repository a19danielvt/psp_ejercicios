
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class InputStreamDemo {
    public static void main(String[] args) {
        try{
            Runtime r = Runtime.getRuntime();
            String[] nargs = {"sh", "-c", "for i in 1 2 3; do exho $i; done"};
            
            Process p = r.exec(nargs);
            
            BufferedReader bf = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            
            while((line = bf.readLine()) != null)
                System.out.println(line);
            
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
