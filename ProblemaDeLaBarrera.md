Los procesos esperan a que el último proceso deje un hueco.

Variables globales para todos los procesos:

~~~java
int n = 10;
int nProcesos = 0; //número de procesos que han llegado a la barrera.
~~~
	
Declaramos dos semáforos:

~~~java
sem barrera = 0; //bloquea
sem mutEx = 1; //exclusión mútua
~~~
~~~java
down(mutEx); //pasa a ser 0
nprocesos += 1; //cada vez que entre un proceso

if(nprocesos < n){			//Si no somos el último proceso
	down(barrera); 			//down(0) -> espera, down(1) ->
	up(mutEx);
}else{					//Si somos el último
	for(int i = 0; i < n; i++){
		up(barrera);
		up(mutEx);
	}
}
~~~

