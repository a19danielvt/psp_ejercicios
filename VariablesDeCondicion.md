#Variables de condición
~~~
monitor sem
int s = k;
condition notZero
operation wait
if(s == 0){
	waitC(notZero)
}
else{
	s--
}
~~~
~~~
sem p
while(true){
	non critical section
	p1: sem wait
	critical section
	p2: sem signal
}
~~~
~~~
sem q
while(true){
	non critical section
	q1: sem wait
	critical section
	q2: sem signal
}
~~~

El waitC siempre bloquea, no es como el wait de un semáforo. Si el signalC encuentra algún proceso bloqueado en la variable condición, lo va a desbloquear. En otro caso NO HACE NADA.

En los semáforos, el signal tiene memoria e incrementa en una unidad el contador del semáforo

Cuando la variable condición es una cola FIFO de procesos:

~~~
waitC(condición){
	añade p a la condición (p state -> bloqueado)
	monitor lock -> bloqueado(signal al semáforo del monitor)
}
~~~
~~~
signal(condición){
	si condición != null
	tomamos la cabecera de la cola FIFO y le asignamos q (q state -> listo)
{
~~~

Hay un proceso en la sección crítica del monitor y que está despertando a un proceso que está en la variable condición. Hay que definir quién es el que continúa ejecutando dentro del monitor, si el proceso señalante o el señalado. En unas implementaciones va a continuar el proceso señalante y en otras el señalado

~~~
p1: sem wait
q1: sem wait
	1, <> (cola de condición)

p2: sem signal
q1: sem wait
	0, <>

p2: sem signal
    blocked
	0, <q>

p1: sem wait
q2: sem signal
	0, <>
~~~
~~~
p1: sem wait
q1: sem wait
	1, <>

p1: sem wait
q2: sem signal
	0, <>

    blocked
q2: sem signal
	0, <p>

p2: sem signal
q2: sem wait
	0, <>
~~~

p2: sem signal|
	      |-> Esto no puede ocurrir
q2: sem signal|
