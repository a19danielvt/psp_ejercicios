#Problema de los filósofos

Existen cinco filósofos que emplean su tiempo entre la meditación y la comida. Se encuentran sentados en una mesa circular en cuyo centro hay un plato de tallarines y disponen de 5 tenedores situados en medio de cada dos de ellos. El principal problema radica en que los tallarines son muy escurridizos y son necesarios dos tenedores para comer, con lo que no podrán comer todos los filósofos a la vez.

Para resolver el problema correctamente, cada filósofo sólo puede comer si se encuentran libres sus dos tenedores adyacentes (aunque  estuvieran libres los otros dos tenedores), y también se debe impedir que ningún filósofo se muera de hambre.

Ejemplo de solución (está mal):

~~~java
sem[] tenedor = new sem[5];
int i;

void filosofo(int i){
    while(true){
	pensar;
	down(tenedor[i]);
	down(tenedor[(i+1)%5]);
	comer;
	up(tenedor[(i+1)%5]);
	up(tenedor[i]);
    }
}

main(){
    for(int i = 0; i < 5; i++){
	tenedor[i] = 1;	
    }

    begin
	filosofo(0);
	filosofo(1);
	filosofo(2);
	filosofo(3);
	filosofo(4);
    end
}
~~~

Se produce interbloqueo al quedar todos los tenedores en 0.

Así está bien (en teoría):

~~~java
filosofo(int i){
	while(true){
		pensar;
		down(tenedor[i]);
		down(tenedor[i+1]);
		comer;
		up(tenedor[i]);
		up(tenedor[i+1]);
	}
}
~~~
