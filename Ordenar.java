import java.util.*;

public class Ordenar{
	public static void main(String[] args){
		ArrayList<Integer> numeros = new ArrayList<>();
		Scanner sc = new Scanner(System.in);

		System.out.println("EL PROGRAMA TERMINA CUANDO INTRODUZCAS UN 0");

		while(true){
			System.out.print("introduce un número: ");
			int n = sc.nextInt();

			if(n == 0)break;

			numeros.add(n);
		}
		int[] n = new int[numeros.size()];

		for(int i = 0; i < numeros.size(); i++){
			n[i] = numeros.get(i);
		}

		Arrays.sort(n);

		String resultado = "";
		for(int i : n)
			resultado += i + " ";

		System.out.println(resultado);
	}
}
