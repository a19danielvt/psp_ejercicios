
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a19danielvt
 */
public class NewClass {
    public static void main(String[] args) throws InterruptedException{
        class Counter{
            int counter = 0;
            public void increment(){counter++;}
            public int get(){return counter;}
        }
        
        final Counter counter = new Counter();
        
        class CountingThread extends Thread{
            @Override
            public void run(){
                for(int i = 0; i < 500000; i++){
                    counter.increment();
                }
            }
        }
        
        CountingThread t1 = new CountingThread();
        CountingThread t2 = new CountingThread();
        t1.start();
        t2.start();
        t1.join(); 
        t2.join();
        System.out.println(counter.get());
    }
}
